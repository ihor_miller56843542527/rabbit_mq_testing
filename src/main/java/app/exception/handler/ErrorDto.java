package app.exception.handler;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Date;

@Data
public class ErrorDto {
    private String data;
    private long timestamp;
    private short code;

    public ErrorDto(String data, HttpStatus status) {
        this.data = data;
        timestamp = new Date().getTime();
        this.code = (short) status.value();
    }
}
