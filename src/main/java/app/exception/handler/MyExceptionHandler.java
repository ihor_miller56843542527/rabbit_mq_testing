package app.exception.handler;

import app.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static app.exception.handler.ErrorConstants.ANY_ERROR;
import static app.exception.handler.ErrorConstants.NOT_FOUND;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestControllerAdvice
public class MyExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ErrorDto anyError(Exception e) {
        return new ErrorDto(ANY_ERROR, INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NotFoundException.class)
    public ErrorDto notFound(NotFoundException e) {
        return new ErrorDto(NOT_FOUND, HttpStatus.NOT_FOUND);
    }
}
