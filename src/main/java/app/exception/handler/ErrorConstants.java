package app.exception.handler;

public class ErrorConstants {
    public static final String NOT_FOUND = "Not found!";
    public static final String ANY_ERROR = "Something went wrong!";
}
