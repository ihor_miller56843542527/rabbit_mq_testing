package app.config;

import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Component
public class RabbitReceiver {
    // There is 'unused' methods. But RabbitMQ API uses it in different situations
    // For each message it's own receiver method
    // If byte[] are come, then method with byte[] parameter
    // Approximately with String and Object (Json)

    public void receiveMessageMethod(byte[] msgBytes) {
        String message = new String(msgBytes, StandardCharsets.UTF_8);
        System.out.println("Bytes (Message): " + message);
    }

    public void receiveMessageMethod(String message) {
        System.out.println("Message: " + message);
    }
}
