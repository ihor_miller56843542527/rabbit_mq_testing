package app.controller;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class MainController {

    private String routingKey;
    private RabbitTemplate rabbitTemplate;
    private TopicExchange topicExchange;

    public MainController(RabbitTemplate rabbitTemplate, @Value("${rbmq.routingKey}") String routingKey, TopicExchange topicExchange) {
        this.rabbitTemplate = rabbitTemplate;
        this.routingKey = routingKey;
        this.topicExchange = topicExchange;
    }

    @GetMapping
    public void hello() {
        // We need to send message, using Topic Name and Routing Key name.
        // Queue is binded via routingKey.
        rabbitTemplate.convertAndSend(topicExchange.getName(), routingKey, "Hello from RabbitMQ!");
    }
}
